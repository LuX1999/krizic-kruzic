﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_ime1 = new System.Windows.Forms.TextBox();
            this.tb_ime2 = new System.Windows.Forms.TextBox();
            this.btn_pocetak = new System.Windows.Forms.Button();
            this.btn_00 = new System.Windows.Forms.Button();
            this.btn_01 = new System.Windows.Forms.Button();
            this.btn_02 = new System.Windows.Forms.Button();
            this.btn_10 = new System.Windows.Forms.Button();
            this.btn_11 = new System.Windows.Forms.Button();
            this.btn_12 = new System.Windows.Forms.Button();
            this.btn_20 = new System.Windows.Forms.Button();
            this.btn_21 = new System.Windows.Forms.Button();
            this.btn_22 = new System.Windows.Forms.Button();
            this.lbl_red = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_ime1 = new System.Windows.Forms.Label();
            this.lbl_ime2 = new System.Windows.Forms.Label();
            this.lbl_rez1 = new System.Windows.Forms.Label();
            this.lbl_rez2 = new System.Windows.Forms.Label();
            this.btn_novaigra = new System.Windows.Forms.Button();
            this.btn_izlaz = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unesi ime prvog igrača: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Unesi ime drugog igrača:";
            // 
            // tb_ime1
            // 
            this.tb_ime1.Location = new System.Drawing.Point(197, 20);
            this.tb_ime1.Name = "tb_ime1";
            this.tb_ime1.Size = new System.Drawing.Size(196, 26);
            this.tb_ime1.TabIndex = 2;
            // 
            // tb_ime2
            // 
            this.tb_ime2.Location = new System.Drawing.Point(197, 63);
            this.tb_ime2.Name = "tb_ime2";
            this.tb_ime2.Size = new System.Drawing.Size(196, 26);
            this.tb_ime2.TabIndex = 3;
            // 
            // btn_pocetak
            // 
            this.btn_pocetak.Location = new System.Drawing.Point(411, 45);
            this.btn_pocetak.Name = "btn_pocetak";
            this.btn_pocetak.Size = new System.Drawing.Size(82, 26);
            this.btn_pocetak.TabIndex = 4;
            this.btn_pocetak.Text = "Započni!";
            this.btn_pocetak.UseVisualStyleBackColor = true;
            this.btn_pocetak.Click += new System.EventHandler(this.btn_pocetak_Click);
            // 
            // btn_00
            // 
            this.btn_00.Location = new System.Drawing.Point(17, 156);
            this.btn_00.Name = "btn_00";
            this.btn_00.Size = new System.Drawing.Size(75, 75);
            this.btn_00.TabIndex = 5;
            this.btn_00.UseVisualStyleBackColor = true;
            this.btn_00.Click += new System.EventHandler(this.btn_00_Click);
            // 
            // btn_01
            // 
            this.btn_01.Location = new System.Drawing.Point(98, 156);
            this.btn_01.Name = "btn_01";
            this.btn_01.Size = new System.Drawing.Size(75, 75);
            this.btn_01.TabIndex = 6;
            this.btn_01.UseVisualStyleBackColor = true;
            this.btn_01.Click += new System.EventHandler(this.btn_01_Click);
            // 
            // btn_02
            // 
            this.btn_02.Location = new System.Drawing.Point(179, 156);
            this.btn_02.Name = "btn_02";
            this.btn_02.Size = new System.Drawing.Size(75, 75);
            this.btn_02.TabIndex = 7;
            this.btn_02.UseVisualStyleBackColor = true;
            this.btn_02.Click += new System.EventHandler(this.btn_02_Click);
            // 
            // btn_10
            // 
            this.btn_10.Location = new System.Drawing.Point(17, 237);
            this.btn_10.Name = "btn_10";
            this.btn_10.Size = new System.Drawing.Size(75, 75);
            this.btn_10.TabIndex = 8;
            this.btn_10.UseVisualStyleBackColor = true;
            this.btn_10.Click += new System.EventHandler(this.btn_10_Click);
            // 
            // btn_11
            // 
            this.btn_11.Location = new System.Drawing.Point(98, 237);
            this.btn_11.Name = "btn_11";
            this.btn_11.Size = new System.Drawing.Size(75, 75);
            this.btn_11.TabIndex = 9;
            this.btn_11.UseVisualStyleBackColor = true;
            this.btn_11.Click += new System.EventHandler(this.btn_11_Click);
            // 
            // btn_12
            // 
            this.btn_12.Location = new System.Drawing.Point(179, 237);
            this.btn_12.Name = "btn_12";
            this.btn_12.Size = new System.Drawing.Size(75, 75);
            this.btn_12.TabIndex = 10;
            this.btn_12.UseVisualStyleBackColor = true;
            this.btn_12.Click += new System.EventHandler(this.btn_12_Click);
            // 
            // btn_20
            // 
            this.btn_20.Location = new System.Drawing.Point(17, 318);
            this.btn_20.Name = "btn_20";
            this.btn_20.Size = new System.Drawing.Size(75, 75);
            this.btn_20.TabIndex = 11;
            this.btn_20.UseVisualStyleBackColor = true;
            this.btn_20.Click += new System.EventHandler(this.btn_20_Click);
            // 
            // btn_21
            // 
            this.btn_21.Location = new System.Drawing.Point(98, 318);
            this.btn_21.Name = "btn_21";
            this.btn_21.Size = new System.Drawing.Size(75, 75);
            this.btn_21.TabIndex = 12;
            this.btn_21.UseVisualStyleBackColor = true;
            this.btn_21.Click += new System.EventHandler(this.btn_21_Click);
            // 
            // btn_22
            // 
            this.btn_22.Location = new System.Drawing.Point(179, 318);
            this.btn_22.Name = "btn_22";
            this.btn_22.Size = new System.Drawing.Size(75, 75);
            this.btn_22.TabIndex = 13;
            this.btn_22.UseVisualStyleBackColor = true;
            this.btn_22.Click += new System.EventHandler(this.btn_22_Click);
            // 
            // lbl_red
            // 
            this.lbl_red.AutoSize = true;
            this.lbl_red.Location = new System.Drawing.Point(13, 119);
            this.lbl_red.Name = "lbl_red";
            this.lbl_red.Size = new System.Drawing.Size(0, 20);
            this.lbl_red.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 421);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "SVEUKUPNI REZULTAT:";
            // 
            // lbl_ime1
            // 
            this.lbl_ime1.AutoSize = true;
            this.lbl_ime1.Location = new System.Drawing.Point(233, 421);
            this.lbl_ime1.Name = "lbl_ime1";
            this.lbl_ime1.Size = new System.Drawing.Size(0, 20);
            this.lbl_ime1.TabIndex = 16;
            // 
            // lbl_ime2
            // 
            this.lbl_ime2.AutoSize = true;
            this.lbl_ime2.Location = new System.Drawing.Point(233, 464);
            this.lbl_ime2.Name = "lbl_ime2";
            this.lbl_ime2.Size = new System.Drawing.Size(0, 20);
            this.lbl_ime2.TabIndex = 17;
            // 
            // lbl_rez1
            // 
            this.lbl_rez1.AutoSize = true;
            this.lbl_rez1.Location = new System.Drawing.Point(344, 421);
            this.lbl_rez1.Name = "lbl_rez1";
            this.lbl_rez1.Size = new System.Drawing.Size(0, 20);
            this.lbl_rez1.TabIndex = 18;
            // 
            // lbl_rez2
            // 
            this.lbl_rez2.AutoSize = true;
            this.lbl_rez2.Location = new System.Drawing.Point(344, 464);
            this.lbl_rez2.Name = "lbl_rez2";
            this.lbl_rez2.Size = new System.Drawing.Size(0, 20);
            this.lbl_rez2.TabIndex = 19;
            // 
            // btn_novaigra
            // 
            this.btn_novaigra.Location = new System.Drawing.Point(277, 156);
            this.btn_novaigra.Name = "btn_novaigra";
            this.btn_novaigra.Size = new System.Drawing.Size(116, 32);
            this.btn_novaigra.TabIndex = 20;
            this.btn_novaigra.Text = "Nova Igra";
            this.btn_novaigra.UseVisualStyleBackColor = true;
            this.btn_novaigra.Click += new System.EventHandler(this.btn_novaigra_Click);
            // 
            // btn_izlaz
            // 
            this.btn_izlaz.Location = new System.Drawing.Point(450, 444);
            this.btn_izlaz.Name = "btn_izlaz";
            this.btn_izlaz.Size = new System.Drawing.Size(75, 40);
            this.btn_izlaz.TabIndex = 21;
            this.btn_izlaz.Text = "Izlaz";
            this.btn_izlaz.UseVisualStyleBackColor = true;
            this.btn_izlaz.Click += new System.EventHandler(this.btn_izlaz_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(194, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(239, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Gubitnik runde je uvijek u sljedećoj prvi na redu...";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 464);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 35);
            this.button1.TabIndex = 23;
            this.button1.Text = "Autor";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(549, 511);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_izlaz);
            this.Controls.Add(this.btn_novaigra);
            this.Controls.Add(this.lbl_rez2);
            this.Controls.Add(this.lbl_rez1);
            this.Controls.Add(this.lbl_ime2);
            this.Controls.Add(this.lbl_ime1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_red);
            this.Controls.Add(this.btn_22);
            this.Controls.Add(this.btn_21);
            this.Controls.Add(this.btn_20);
            this.Controls.Add(this.btn_12);
            this.Controls.Add(this.btn_11);
            this.Controls.Add(this.btn_10);
            this.Controls.Add(this.btn_02);
            this.Controls.Add(this.btn_01);
            this.Controls.Add(this.btn_00);
            this.Controls.Add(this.btn_pocetak);
            this.Controls.Add(this.tb_ime2);
            this.Controls.Add(this.tb_ime1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Križić - Kružić";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_ime1;
        private System.Windows.Forms.TextBox tb_ime2;
        private System.Windows.Forms.Button btn_pocetak;
        private System.Windows.Forms.Button btn_00;
        private System.Windows.Forms.Button btn_01;
        private System.Windows.Forms.Button btn_02;
        private System.Windows.Forms.Button btn_10;
        private System.Windows.Forms.Button btn_11;
        private System.Windows.Forms.Button btn_12;
        private System.Windows.Forms.Button btn_20;
        private System.Windows.Forms.Button btn_21;
        private System.Windows.Forms.Button btn_22;
        private System.Windows.Forms.Label lbl_red;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_ime1;
        private System.Windows.Forms.Label lbl_ime2;
        private System.Windows.Forms.Label lbl_rez1;
        private System.Windows.Forms.Label lbl_rez2;
        private System.Windows.Forms.Button btn_novaigra;
        private System.Windows.Forms.Button btn_izlaz;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}

