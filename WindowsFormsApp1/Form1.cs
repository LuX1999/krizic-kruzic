﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        //matrica stanja koja nam sluzi da bi provjeravali je li koji od igraca pobjedio
        int [,]matrica_stanja = new int[3, 3];
        int broj_odabira = 0;
        bool zapoceta_igra = false;

        //stanje pobjede je stanje kada je jedan od dva igraca pobjedio i tu se prekida igra
        bool stanje_pobjede = false;
        Igrac igrac1 = new Igrac();
        Igrac igrac2 = new Igrac();
        public Form1()
        {
            InitializeComponent();
        }
        class Igrac
        {
            private string ime;
            private int broj_pobjeda = 0;
            bool red = false;
            public Igrac()
            {
                ime = "NoInfo";
                broj_pobjeda = 0;
            }
            public void setIme(string ime)
            {
                this.ime = ime;
            }
            public void setBrojPobjeda(int broj)
            {
                broj_pobjeda = broj;
            }
            public void setRed(bool set)
            {
                red = set;
            }
            public int getBrojPobjeda()
            {
                return broj_pobjeda;
            }
            public string getIme()
            {
                return ime;
            }
            public bool getRed()
            {
                return red;
            }
            public void Dodaj_Pobjedu()
            {
                broj_pobjeda = broj_pobjeda + 1;
            }
            
        }

        private void btn_pocetak_Click(object sender, EventArgs e)
        {
            if (tb_ime1.Text == "" && tb_ime2.Text == "")
            {
                MessageBox.Show("Nisu unešena imena igrača!");
            }
            else if (tb_ime1.Text == "")
            {
                MessageBox.Show("Nije unešeno ime prvog igrača!");
            }
            else if (tb_ime2.Text == "")
            {
                MessageBox.Show("Nije unešeno ime drugog igrača!");
            }
            else if (tb_ime1.Text == tb_ime2.Text)
            {
                MessageBox.Show("Imena igrača ne mogu biti jednaka!");
            }
            else
            {
                if (zapoceta_igra == false)
                {
                    zapoceta_igra = true;
                    igrac1.setIme(tb_ime1.Text);
                    igrac2.setIme(tb_ime2.Text);
                    lbl_rez1.Text = igrac1.getBrojPobjeda().ToString();
                    lbl_rez2.Text = igrac2.getBrojPobjeda().ToString();
                    lbl_ime1.Text = igrac1.getIme();
                    lbl_ime2.Text = igrac2.getIme();

                    //nasumicno cemo birati tko ce prvi biti na redu...

                    Random r = new Random();
                    int Random = r.Next(1, 10);
                    if (Random % 2 == 0)
                    {
                        lbl_red.Text = "Na redu je " + igrac1.getIme();
                        igrac1.setRed(true);
                        igrac2.setRed(false);
                       
                    }
                    else
                    {
                        lbl_red.Text = "Na redu je " + igrac2.getIme();
                        igrac2.setRed(true);
                        igrac1.setRed(false);
                    }
                 }
                else
                {
                    MessageBox.Show("Igra je već započeta!");
                }
            }
        }

        //provjera je li igra zavrsena nerijeseno
        public bool provjera_za_nerjeseno(int[,] matrica)
        {
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    if (matrica[i,j]==0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool provjera_pobjede(int [,]matrica)
        {
            //provjeravamo sve kombinacije pobjede(ima ih 8)...

            if ((matrica[0, 0] == 1 && matrica[0, 1] == 1 && matrica[0, 2] == 1)|| (matrica[0, 0] == 2 && matrica[0, 1] == 2 && matrica[0, 2] == 2))
            {
                return true;
            }
            if((matrica[1, 0] == 1 && matrica[1, 1] == 1 && matrica[1, 2] == 1)|| (matrica[1, 0] == 2 && matrica[1, 1] == 2 && matrica[1, 2] == 2))
            {
                return true;
            }
            if((matrica[2, 0] == 1 && matrica[2, 1] == 1 && matrica[2, 2] == 1)|| (matrica[2, 0] == 2 && matrica[2, 1] == 2 && matrica[2, 2] == 2))
            {
                return true;
            }
            if((matrica[0, 2] == 1 && matrica[1, 2] == 1 && matrica[2, 2] == 1)|| (matrica[0, 2] == 2 && matrica[1, 2] == 2 && matrica[2, 2] == 2))
            {
                return true;
            }
            if((matrica[0, 1] == 1 && matrica[1, 1] == 1 && matrica[2, 1] == 1)|| (matrica[0, 1] == 2 && matrica[1, 1] == 2 && matrica[2, 1] == 2))
            {
                return true;
            }
            if((matrica[0, 0] == 1 && matrica[1, 0] == 1 && matrica[2, 0] == 1)|| (matrica[0, 0] == 2 && matrica[1, 0] == 2 && matrica[2, 0] == 2))
            {
                return true;
            }
            if((matrica[0, 0] == 1 && matrica[1, 1] == 1 && matrica[2, 2] == 1)|| (matrica[0, 0] == 2 && matrica[1, 1] == 2 && matrica[2, 2] == 2))
            {
                return true;
            }
            if((matrica[0, 2] == 1 && matrica[1, 1] == 1 && matrica[2, 0] == 1)|| (matrica[0, 2] == 2 && matrica[1, 1] == 2 && matrica[2, 0] == 2))
            {
                return true;
            }
            return false;
        }

        private void btn_00_Click(object sender, EventArgs e)
        {
            if (btn_00.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        //stanje 1 je ako je igrac broj 2 zauzeo to mjesto u polju...
                        matrica_stanja[0, 0] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_00.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        //stanje 2 je ako je igrac broj 2 zauzeo to mjesto u polju...
                        matrica_stanja[0, 0] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_00.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    //pobjednik je onaj tko je bio zadnji na redu...
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
            
        }

        private void btn_01_Click(object sender, EventArgs e)
        {
            if (btn_01.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[0, 1] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_01.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[0, 1] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_01.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_02_Click(object sender, EventArgs e)
        {
            if (btn_02.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[0, 2] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_02.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[0, 2] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_02.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_10_Click(object sender, EventArgs e)
        {
            if (btn_10.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[1, 0] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_10.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[1, 0] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_10.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_11_Click(object sender, EventArgs e)
        {
            if (btn_11.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[1, 1] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_11.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[1, 1] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_11.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_12_Click(object sender, EventArgs e)
        {
            if (btn_12.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[1, 2] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_12.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[1, 2] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_12.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_20_Click(object sender, EventArgs e)
        {
            if (btn_20.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[2, 0] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_20.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[2, 0] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_20.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_21_Click(object sender, EventArgs e)
        {
            if (btn_21.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[2, 1] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_21.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[2, 1] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_21.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }
            }
        }

        private void btn_22_Click(object sender, EventArgs e)
        {
            if (btn_22.Text != "")
            {
                MessageBox.Show("Ovo polje je zauzeto!");
            }
            else
            {
                if (stanje_pobjede == false)
                {
                    if (igrac1.getRed() == true)
                    {
                        matrica_stanja[2, 2] = 1;
                        lbl_red.Text = "Na redu je " + igrac2.getIme() + ".";
                        btn_22.Text = "X";
                        igrac1.setRed(false);
                        igrac2.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                    else if (igrac2.getRed() == true)
                    {
                        matrica_stanja[2, 2] = 2;
                        lbl_red.Text = "Na redu je " + igrac1.getIme() + ".";
                        btn_22.Text = "O";
                        igrac2.setRed(false);
                        igrac1.setRed(true);
                        stanje_pobjede = provjera_pobjede(matrica_stanja);
                    }
                }
                if (stanje_pobjede == true)
                {
                    if (igrac1.getRed() == false)
                    {
                        igrac1.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac1.getIme() + "!");
                    }
                    if (igrac2.getRed() == false)
                    {
                        igrac2.Dodaj_Pobjedu();
                        MessageBox.Show("Pobjednik je " + igrac2.getIme() + "!");
                    }
                }
                else if (provjera_za_nerjeseno(matrica_stanja) == true)
                {
                    MessageBox.Show("Remi!");
                }

            }
            }

        private void btn_novaigra_Click(object sender, EventArgs e)
        {
            lbl_rez1.Text = igrac1.getBrojPobjeda().ToString();
            lbl_rez2.Text = igrac2.getBrojPobjeda().ToString();
            btn_00.Text = "";
            btn_01.Text = "";
            btn_02.Text = "";
            btn_10.Text = "";
            btn_11.Text = "";
            btn_12.Text = "";
            btn_20.Text = "";
            btn_21.Text = "";
            btn_22.Text = "";
            stanje_pobjede = false;

            //resetiramo cijelu matricu stanja na 0, jer je 0 neutralna dok je 1 oznaka igraca1,a 2 oznaka igraca2
            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    matrica_stanja[i, j] = 0;
                }
            }
        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Luka Lovretić\nSveučilišni preddiplomski studij računarstva 2.god.");
        }
    }
}
